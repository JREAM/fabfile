from fabric.api import *

"""
Required:
---------
- Python Fabric Installed
- Remote Linux/MySQL/Git + SSH Setup

GIT Lock File Workaround:
-------------------------
1: Add user to www-data file (or your Apache Group)
2: Grant MODIFY/DELETE permissions file(s) my USER owns && everyone in the GROUP:
    $ chmod -R ug+rw .git
"""

# SSH User & Host
env.user = 'root'
env.host_string = '99.99.99.99'

# DB Settings
dbfile = 'schema.sql'
dev = dict(
    path='/c/workspace/',
    host='localhost',
    port=3306,
    user='root',
    passwd='',
    schema='dbname',
)
production = dict(
    path='/var/www/workspace',
    host='master.999999.us-east-1.rds.amazonaws.com',
    port=3306,
    user='root',
    passwd='',
    schema='dbname'
)

"""
GIT Add/Update/Commit/Push in one line

$ fab commit
$ fab commit:branch
"""
def commit(branch = 'master'):
    local('git add -u')
    local('git add .')
    message = prompt("Commit Message: ")
    local('git commit -m "{0}"'.format(message))
    local('git push origin {0}'.format(branch))

"""
Does a git pull
It's the lazy mans way of saving on keystrokes

$ fab pull
$ fab pull:branch
"""
def pull(branch = 'master'):
    local('git pull origin {0}'.format(branch))

"""
Deploys the branch to the Remote Host

$ fab deploy
$ fab deploy:branch
"""
def deploy(branch = 'master'):
    with cd(production['path']):
        run("git pull origin {0}".format(branch))

"""
Deploys Database to the Remote Host
- Let the SQL file do the DB Drop, or anything it needs

$ fab deploydb
$ fab debploydb:other.sql
"""
def deploydb():
    with cd(production['path']):
        run("mysql -h {0} -P {1} -u {2} -p{3} {4} < {5}".format(
                production['host'],
                production['port'],
                production['user'],
                production['passwd'],
                production['schema'],
                dbfile
            )
        )

"""
Dumps database into SQL file

$ fab dumpdb
"""
def dumpdb():
    if len(dev['passwd']) > 0:
        local("mysqldump -h {0} -P {1} -u {2} -p{3} {4} > {5}".format(
                dev['host'],
                dev['port'],
                dev['user'],
                dev['passwd'],
                dev['schema'],
                dbfile
            )
        );
    else:
        local("mysqldump -h {0} -P {1} -u {2} {3} > {4}".format(
                dev['host'],
                dev['port'],
                dev['user'],
                dev['schema'],
                dbfile
            )
        );